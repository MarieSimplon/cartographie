var url = 'json/filaire_gc.json';
//'https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=t_troncon_cycle&rows=306&facet=typeamenag' // (here local) defines the URL where we can find our JSON data
//https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=filaire_gc&rows=1280&facet=nomvoie&facet=communed&facet=domani2017&refine.domani2017=Communale&refine.communed=CHAMBERY
//https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=t_troncon_cycle&facet=typeamenag
//doesn't work online ???
var request = new XMLHttpRequest(); // initializes the remote request 
request.open('GET', url, true);

request.onload = function () {
    let adresse = document.getElementById('adresse'); // targets the select element
    adresse.length = 0; // clears any options in the element
    let commune = document.getElementById('commune');
    commune.length = 0;

    let AdDefaultOption = document.createElement('option'); // appends our default option
    AdDefaultOption.text = 'Choix nom de rue';
    let CoDefaultOption = document.createElement('option');
    CoDefaultOption.text = 'Choix commune';

    adresse.add(AdDefaultOption);
    adresse.selectedIndex = 0;
    commune.add(CoDefaultOption);
    commune.selectedIndex = 0;

    if (request.status === 200) {
        var data = JSON.parse(request.responseText);

        for (let i = 0; i < data.length; i++) { // creates an option element for each entry found and adds it to the select list
            let AdOption = document.createElement('option');
            AdOption.text = data[i].fields.nomvoie;
            AdOption.value = data[i].fields.nomvoie;
            adresse.add(AdOption);

        }
        let coOption = document.createElement('option');
        coOption.text = data[0].fields.communed;
        coOption.value = data[0].fields.communed;
        commune.add(coOption);
    } else {
        // Reached the server, but it returned an error
    }
}
request.onerror = function () {
    console.error('An error occurred fetching the JSON from ' + url);
};

request.send(); // sends the remote request

var request2 = new XMLHttpRequest(); // initializes the remote request 
request2.open('GET', url, true);

request2.onload = function () {
    let adresse = document.getElementById('adresse2'); // targets the select element
    adresse.length = 0; // clears any options in the element
    let commune = document.getElementById('commune2');
    commune.length = 0;

    let AdDefaultOption = document.createElement('option'); // appends our default option
    AdDefaultOption.text = 'Choix nom de rue';
    let CoDefaultOption = document.createElement('option');
    CoDefaultOption.text = 'Choix commune';

    adresse.add(AdDefaultOption);
    adresse.selectedIndex = 0;
    commune.add(CoDefaultOption);
    commune.selectedIndex = 0;

    if (request2.status === 200) {
        var data2 = JSON.parse(request2.responseText);

        for (let i = 0; i < data2.length; i++) { // creates an option element for each entry found and adds it to the select list
            let AdOption = document.createElement('option');
            AdOption.text = data2[i].fields.nomvoie;
            AdOption.value = data2[i].fields.nomvoie;
            adresse.add(AdOption);
            
        }
        let coOption = document.createElement('option');
        coOption.text = data2[0].fields.communed;
        coOption.value = data2[0].fields.communed;
        commune.add(coOption);
    } else {
        // Reached the server, but it returned an error
    }
}
request2.onerror = function () {
    console.error('An error occurred fetching the JSON from ' + url);
};

request2.send(); // sends the remote request
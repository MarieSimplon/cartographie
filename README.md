Projet Cartographie

En ligne https://mobilite-verte-chambery.netlify.com/

L’objectif est de programmer une application de cartographie permettant d’exploiter des données ouvertes « open data » proposées par la ville de Chambéry.

https://donnees.grandchambery.fr/

Durée : moins de 2 semaines
Groupe : projet individuel

Réalisation : 
    • Maquetter une application et réalisation d’un scénario utilisateur
    • Approfondissement des mécanismes asynchrones et plus particulièrement de type Ajax via l’utilisation de l’API JavaScript « Fetch »
    • Découverte de « open data » et de l’utilisation de services web exploitant un type d’architecture REST
    • Découverte de « OpenStreetMap » et de son utilisation par l’intermédiaire de la librairie de carte interactive Leaflet
    
Compétences :
    • Front-end : Maquetter une application
    • Front-end : Réaliser une interface utilisateur web statique et adaptable
    • Front-end : Développer une interface utilisateur web dynamique

Contraintes :
    • L’ensemble du code source (nom des variables, fonctions) et des commentaires doivent être en anglais.
    • Les données « open data » doivent obligatoirement être exploitées via les services web REST par l’intermédiaire de fetch.
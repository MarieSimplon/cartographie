//personalized icon 
var icoH2 = L.icon({ //hydrogen station
    iconUrl: 'img/icoH23.png',
    iconSize: [40, 40],
});
var icoVelo = L.icon({ //Velostation
    iconUrl: 'img/icoVelo.png',
    iconSize: [60, 40],
});
var icoTicket = L.icon({ //ticket sale
    iconUrl: 'img/icoTicket.png',
    iconSize: [40, 40],
})
var icoBusStop6 = L.icon({ //bus stop
    iconUrl: 'img/icoBusStop.png',
    iconSize: [40, 40],
})

//geolocation button 
function initElement() {
    let p = document.getElementById("geolocation");
    p.onclick = currentLocation;
}

function currentLocation() {
    if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition((function (position) {
            var marker = L.marker([position.coords.latitude,
            position.coords.longitude
            ]).addTo(mymap);
            marker.bindPopup("Ma position : <br> Latitude : " +
                position.coords.latitude + "<br> Longitude : " +
                position.coords.longitude).openPopup();
        }));
    } else {
        alert("La géolocalisation n'est pas supportée par ce navigateur.");
    }
}
//display the map
var mymap = L.map('mapid').setView([45.564601, 5.917781], 15);
L.tileLayer('http://{s}.tile.osm.org/{z}/{x}/{y}.png', {
    attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
}).addTo(mymap);

//display chambery's marker

var markerChy = L.marker([45.564601, 5.917781])
    .bindPopup("Bienvenue à Chambéry")
    .addTo(mymap);

function relachement(e) {
    markerDraggable.getPopup().setContent('' + markerDraggable.getLatLng());
    markerDraggable.openPopup();
}

var repere = L.marker([45.559000, 5.917000])
    .bindPopup("JE SUIS REPERE")
    .addTo(mymap)

//twist the coordinates to display it correctly (cf circuit)
function reverseCoordinateInList(coordinateList) {
    var cordarray = [];
    coordinateList.map(function (cord) {
        return cord.reverse();
    });
    return coordinateList;
}

//analyze
function analyze(list) {
    let good = 0;

    for (let i = 0; i < list.length; i++) {
        for (let j = 0; j < list[i].length; j++) {
            if (list[i][j][0] > list[i][j][1]) {
                if (!good) {
                    console.log("good:" + i + "," + j);
                    good = 1;
                }
            } else {
                if (good) {
                    console.log("not good:" + i + "," + j);
                    good = 0;
                }
            }
        }
    }
}

// reload the entire page (= I'M FED UP WITH THIS)
function reset() {
    location.reload(true);
}

// DISPLAY H2 STATION - chambery's open data
var hydro = 'https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=station-hydrogene-cgle-20181023&facet=n_station&facet=ad_station&facet=commune';

document.getElementById('customCheck1').addEventListener('click', function () {
    //Call the fetch function passing the URL of the API as a parameter
    fetch(hydro)
        //Code for handling the data from the API
        .then(resp => {
            //In this case return a JSON
            return resp.json();
        })
        .then(response => {
            //Loop on each records
            for (let i = 0; i < response.records.length; i++) {
                //localisation marker with a pop up when the user clicks on the marker
                L.marker(response.records[i].fields.localisation, {
                    icon: icoH2
                })
                    .bindPopup("Station Hydrogène pour Vélos électriques</br>" +
                        response.records[i].fields.n_station + '</br>' +
                        response.records[i].fields.ad_station)
                    .addTo(mymap)
            };
        })
});

// DISPLAY VELO STATION - chambery's open data
var velo = 'https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=points-de-ventes-titres-de-transport-du-stac&facet=libelle&facet=commune&refine.libelle=V%C3%A9lostation';

document.getElementById('customCheck2').addEventListener('click', function () {
    //Call the fetch function passing the URL of the API as a parameter
    fetch(velo)
        //Code for handling the data from the API
        .then(resp => {
            //In this case return a JSON
            return resp.json();
        })
        .then(response => {
            //Loop on each records
            for (let i = 0; i < response.records.length; i++) {
                //localisation marker with a pop up when the user clicks on the marker
                L.marker(response.records[i].fields.localisation, {
                    icon: icoVelo
                })
                    .bindPopup(response.records[i].fields.libelle + '</br>' +
                        response.records[i].fields.adresse + "<br>" +
                        response.records[i].fields.commune)
                    .addTo(mymap)
            };
        })
})

//DISPLAY THE CYCLE ROADS - chambery's open data
var cyclable = 'https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=t_troncon_cycle&facet=typeamenag&rows=306';
document.getElementById('customCheck3').addEventListener('click', function () {
    fetch(cyclable)
        //Code for handling the data from the API
        .then(resp => {
            //In this case return a JSON
            return resp.json();
        })
        .then(response => {
            //Loop on each records
            for (let i = 0; i < response.records.length; i++) {
                //localisation marker with a pop up when the user clicks on the marker
                var coordinates = response.records[i].fields.geo_shape.coordinates;
                var coordinates_revers = coordinates.map(function (coord) {
                    return coord.reverse();
                })
                L.polyline(coordinates_revers, {
                    color: '#00c672'
                }).addTo(mymap);
            };
        })
})

// DISPLAY BUS STAC TICKET SALE - chambery's open data
var ticket = 'https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=points-de-ventes-titres-de-transport-du-stac&rows=27&facet=libelle&facet=commune';

document.getElementById('customCheck4').addEventListener('click', function () {
    //Call the fetch function passing the URL of the API as a parameter
    fetch(ticket)
        //Code for handling the data from the API
        .then(resp => {
            //In this case return a JSON
            return resp.json();
        })
        .then(response => {
            //Loop on each records
            for (let i = 0; i < response.records.length; i++) {
                //localisation marker with a pop up when the user clicks on the marker
                L.marker(response.records[i].fields.localisation, {
                    icon: icoTicket
                })
                    .bindPopup(response.records[i].fields.libelle + '</br>' +
                        response.records[i].fields.adresse + '</br>' +
                        response.records[i].fields.code_postal + " " + response.records[i].fields.commune + '</br>' + '</br>' +
                        response.records[i].fields.info)
                    .addTo(mymap);
            };
        })
})

// DISPLAY BUS LINE CIRCUIT - chambery's open data
var circuitBus = "https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=t_troncon_line&facet=nom_ligne&rows=24"

document.getElementById('customCheck5').addEventListener('click', function () {
    //Call the fetch function passing the URL of the API as a parameter
    //function Bus() {
    fetch(circuitBus)
        //Code for handling the data from the API
        .then(resp => {
            return resp.json()
        })
        .then(response => {
            for (let i = 0; i < response.records.length; i++) {
                var multiCoordinateList = response.records[i].fields.geo_shape.coordinates;

                for (let j = 0; j < multiCoordinateList.length; j++) {
                    L.polyline(reverseCoordinateInList(multiCoordinateList[j]))
                        .addTo(mymap);
                }
            }
        });
})

// DISPLAY BUS STOP - chambery's open data
var busStop = 'https://donnees.grandchambery.fr/api/records/1.0/search/?dataset=t_troncon_shape&rows=467&facet=code_ligne&facet=commune&facet=nom';

document.getElementById('customCheck6').addEventListener('click', function () {
    fetch(busStop)
        //Code for handling the data from the API
        .then(resp => {
            //In this case return a JSON
            return resp.json();
        })
        .then(response => {
            //Loop on each records
            for (let i = 0; i < response.records.length; i++) {
                //localisation marker with a pop up when the user clicks on the marker
                L.marker(response.records[i].fields.geo_point_2d, {
                    icon: icoBusStop6
                })
                    .bindPopup(
                        "Ligne : " + response.records[i].fields.code_ligne + '</br>' + //line name                    
                        "Nom de l'arrêt : " + response.records[i].fields.nom + "<br>" + //bus stop name
                        "Commune : " + response.records[i].fields.commune)// tells on which city
                    .addTo(mymap);
            };
        })
})
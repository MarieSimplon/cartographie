const url = 'filaire_gc.json'; // (here local) defines the URL where we can find our JSON data

const request = new XMLHttpRequest();              // initializes the remote request 
request.open('GET', url, true);

request.onload = function() {
  let adresse = document.getElementById('adresse'); // targets the select element
  adresse.length = 0;                               // clears any options in the element
  let commune = document.getElementById('commune'); 
  commune.length = 0;                               
  
  let AdDefaultOption = document.createElement('option'); // appends our default option
  AdDefaultOption.text = 'Choisissez un nom de rue';
  let CoDefaultOption = document.createElement('option'); 
  CoDefaultOption.text = 'Choisissez une commune';
  
  adresse.add(AdDefaultOption);
  adresse.selectedIndex = 0;
  commune.add(CoDefaultOption);
  commune.selectedIndex = 0;

  if (request.status === 200) {
    const data = JSON.parse(request.responseText);

    for (let i = 0; i < data.length; i++) { // creates an option element for each entry found and adds it to the select list
      let AdOption = document.createElement('option');
      AdOption.text = data[i].fields.nomvoie;
      AdOption.value = data[i].fields.nomvoie; 
      
      //option.value = data[i].fields.nomvoie.substr(0); //DOES NOT WORK substr(0,2)? substring(1,2)? str.substr(1, 2)?
      
      adresse.add(AdOption);

      let coOption = document.createElement('option');
      coOption.text = data[i].fields.communed;
      coOption.value = data[i].fields.communed; 
      commune.add(coOption);
    }
   } else {
    // Reached the server, but it returned an error
  }   
}
request.onerror = function() {
  console.error('An error occurred fetching the JSON from ' + url);
};

request.send(); // sends the remote request







